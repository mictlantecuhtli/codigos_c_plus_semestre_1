/*
 * Autor: Fatima Azucena MC
 * Fecha: 20_01_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
using namespace std;

int main (){

	//Declaración variables
	int i;
	int edad;
	int sumaEdad = 0;
	int promedioEdades;

	for ( i = 1; i <= 3; i++){
		cout<<"Digite la edad del alumno "<<i<<": ";
		cin>>edad;
		sumaEdad = sumaEdad + edad;
	}
	promedioEdades = sumaEdad / 3;
	cout<<"El promedio de edades es de: "<<promedioEdades<<"\n";

}
