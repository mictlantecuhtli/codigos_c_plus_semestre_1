#include <iostream>
#include <string>

class Animal {
protected:
    std::string nombre;

public:
    Animal(const std::string& nombre) : nombre(nombre) {}

    // Método virtual para permitir el polimorfismo
    virtual void sonido() const = 0; // Es un animal genérico, así que no tiene un sonido específico

    // Método común a todos los animales
    void saludar() const {
        std::cout << "Hola, soy un " << nombre << ". ";
        sonido();
        std::cout << std::endl;
    }
};

class Perro : public Animal {
public:
    Perro(const std::string& nombre) : Animal(nombre) {}

    // Implementación específica para el sonido del perro
    void sonido() const override {
        std::cout << "¡Guau guau!" << std::endl;
    }
};

class Gato : public Animal {
public:
    Gato(const std::string& nombre) : Animal(nombre) {}

    // Implementación específica para el sonido del gato
    void sonido() const override {
        std::cout << "¡Miau miau!" << std::endl;
    }
};

int main() {
    Animal* animal1 = new Perro("Firulais");
    Animal* animal2 = new Gato("Garfield");

    animal1->saludar(); // Llama al método del perro
    animal2->saludar(); // Llama al método del gato

    delete animal1;
    delete animal2;

    return 0;
}

