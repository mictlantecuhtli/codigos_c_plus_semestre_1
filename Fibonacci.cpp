// Autor: Fátima Azucena MC
// Fecha: 31_01_2023
// Correo: fatimaazucenamartinez@gmail.com

#include <iostream>
using namespace std;

int main(){

	int numero;
	int i;
	int a;
	int b;
	int c;
	int d;
	int x = 0;
	int y = 1;
	int z = 1;

	cout<<"Ingrese el número para la serie de Fibonacci: ";
	cin>>numero;
	cout<<"\n";

	for ( i = 1; i <= numero; i++){
		c = 1;
		for ( a = 1; a < ( numero - i + 1 ); a++){
			cout <<"-";
		}

		for ( b = 1; b <= i; b++)
		{ 
			cout << c << " ";
			c = c * (i - b) / b;
		}
		cout << "\n";
	}

	cout<<"\n\nSucesión\n\n";
	
	for ( d = 1; d <= numero; d++ ){
		z = ( x + y );
		cout<<z<<" ";
		x = y;
		y = z;
	}
	
	cout<<"\n";

}
