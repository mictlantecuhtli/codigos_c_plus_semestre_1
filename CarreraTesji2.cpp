/*
 * Autor: Fatima Azucena MC
 * Fecha: 20_01_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
using namespace std;

int main (){

	//Declaración de variables
	int opcion;
	int cred = 0;
	int taller;
	int carrera;
	int lugar;

	cout<<"¿Participaste en el desfile?\n1.-Si\n0.-No\nDigite su opción: ";
	cin>> opcion;

	if ( opcion == 1 ) {
		cred = cred + 1;
		cout<<"Ganaste un crédito,¿En qué taller estas inscrito?: ";
		cout<<"\n1.-Danza\n2.-Basquetblo\n3.-Futblo\n4.-TKD\n5.-Voliebol\nDigite su respuesta: ";
		cin>>taller;

		if ( taller == 1 ){
			cout<<"Uniforme escolar\n";
		}
		else if ( taller == 2 ){
			cout<<"Blanco\n";
		}
		else if ( taller == 3 ){
			cout<<"Verde\n";
		}
		else if ( taller == 4 ){
			cout<<"Uniforme TKD\n";
		}
		else if ( taller == 5 ){
			cout<<"Rojo\n";
		}
		else {
			cout<<"Opción inválida\n";
		}

	}
	else {
		cout<<"No tienes ningún crédito, ";
		cred = 0;
	}

	cout<<"¿Participaste en la carrera?:\n1.-Si\n0.-No\nDigite su respuesta: ";
	cin>>carrera;

	if ( carrera == 1 ){
		cred = cred + 1;
	}
	else {
		cred = 0;
	}

	cout<<"¿Quedaste en uno de los 3 primeros lugares?:\n1.-Si\n0.-No\nDigite su respuesta: ";
	cin>>lugar;

	if ( lugar == 1 ){
		cred = cred + 1;
	}
	else {
		cred = 0;
	}
	
	cout<<"El total de créditos es: "<<cred<<"\n";

}
