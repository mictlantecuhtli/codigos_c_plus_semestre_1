/*
 * Autor: Fatima Azucena MC
 * Fecha: 20_01_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
using namespace std;

int main (){

	//Declaración de variables
	int factorial;
	int i;
	int z = 1;
	int resFac;

	cout<<"¿De qué número desea ver su factorial?: ";
	cin>>factorial;

	for ( i = 1; i <= factorial; i++ ){
		resFac = z * i;
		cout<<z<<" * "<<i<<" = "<<resFac<<"\n";
		z = resFac;
	}
	cout<<"El factorial de "<<factorial<<" es: "<<z<<"\n";

}
