// Fecha: 18_07_23
// Autor: Fátima Azucena MC
// Correo: fatimaazucenamartinez274@gmail.com

// Calcular en área bajo la curva mediante las sumas de Riemann

#include <iostream>
using namespace std;

double funcion(double x){ // Inicio función que evalúa a f(x)
    return (x * x); // x^2
} // Fin función que evalúa a f(x)

double sumaRiemann(double a, double b, int n){ // Inicio función sumaRiemann  
    double h = (b - a) / n;  // Ancho de cada subintervalo
    double suma = 0;
    double x;
    int i;
     
    // Calcular la suma de cada rectangula
    for (i = 0; i < n; i++) {
        x = a + i * h;  // Punto dentro del subintervalo
        suma += funcion(x) * h;
    }

    return suma;

} // Fin función sumaRiemann

int main() { // Inicio método principal
    // Declaración de variables
    double a = 0; // Limite inferior
    double b = 1; // Limite superior
    int n = 100; // Número de rectángulos.
    double resultado;
    // Llamada a la función sumaRiemann
    resultado = sumaRiemann(a, b, n);

    cout<<"El resultado de la suma de Riemann es: "<<resultado<<"\n";

} // Fin método principal
