/*
 * Autor: Fátima Azucena Martínez Cadena
 * Fecha: 12_12_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
using namespace std;

int main() {

	//Declaración de variables
	int calificaciones[6][6];
        char  materias [][50] = {"Cálculo Diferencial          ", "Fundamentos de programación  ", "Química                      ", 
	"Fundamentos de investigación ","Matemáticas Discretas        ", "Desarrollo Sustentable       "};
        int i;
        int y;
        int a;
        int b;
        int promedioFinal;
        int promedioUnidades = 0;

	for ( i = 0; i < 6; i++ ) {
                int promedioMateria = 0;
                int sumaCalificaciones = 0;
                for ( y = 0; y < 5; y++ ) {
                        cout<<"Ingrese la calificación de la unidad "<<y<<" de la materia "<<materias[i]<<": ";
                        cin>>calificaciones[i][y];
                        sumaCalificaciones += calificaciones[i][y];
                }
                promedioMateria = sumaCalificaciones / 5;
                promedioUnidades += promedioMateria;
		calificaciones[i][5] = promedioMateria;
		//cout<<"|      "<<calificaciones[i][5];
		//cout<<"\n";
        }
	cout<<"----------------------------------------------------------------------------------------------------------------------\n";
        cout<<"\t\t\t\tUnidad 1\tUnidad 2\tUnidad 3\tUnidad 4\tUnidad 5\tPromedio\n";
        cout<<"----------------------------------------------------------------------------------------------------------------------\n";
	for ( a = 0; a < 6; a++ ){
		cout<<materias[a];
		for ( b = 0; b < 5; b++ ){
			cout<<"\t|   "<<calificaciones[a][b]<<"\t";
		}
     		cout<<"|      "<<calificaciones[a][5];
                cout<<"\n";
	}
        promedioFinal = promedioUnidades / 6;
        cout<<"----------------------------------------------------------------------------------------------------------------------\n";
	cout<<"El promedio general es de: "<<promedioFinal<<"\n";
	cout<<"----------------------------------------------------------------------------------------------------------------------\n";
}

