/*
 * Autor: Fatima Azucena MC
 * Fecha: 20_01_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
using namespace std;

int main (){

	//Declaración de variables
	int i;
	int y;
	int x;
	int j;

	for ( i = 1; i <= 8; i++) {
		for ( y = 1; y <= 8 - i; y++) {
			cout<<" ";
		}
		for ( x = 1; x <= ( i * 2 ) - 1; x++) {
			cout<<"*";
		}
		cout<<"\n";
	}
	for ( j = 1; j <= 4; j++ ){
		cout<<"      * * \n";
	}	

}
