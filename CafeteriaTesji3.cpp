/*
 * Autor: Fatima Azucena MC
 * Fecha: 20_01_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
using namespace std;

int main(){

	//Declaración de variables
	int color;
	int totalPagar = 0;
	int descuento = 0;
	int totalPagarDescuento = 0;

	cout<<"¿De qué color es la pelota que acabas de sacar?:\n1.-Verde\n2.-Amarilla\n3.-Roja";
	cout<<"\nDigite su respuesta: ";
	cin>>color;

	if ( color == 1 ){
		cout<<"Felicidades, tienes un descuento del 10 porciento";
		cout<<"\n¿Cuál es el monto de tu compra?: ";
		cin>>totalPagar;
		descuento = totalPagar * 0.10;
		totalPagarDescuento = totalPagar - descuento;
	}
	else if ( color == 2 ){
		cout<<"Felicidades, tienes un descuento del 5 porciento";
                cout<<"\n¿Cuál es el monto de tu compra?: ";
                cin>>totalPagar;
                descuento = totalPagar * 0.05;
                totalPagarDescuento = totalPagar - descuento;
	}
	else if ( color == 3 ){
		cout<<"Felicidades, tienes un descuento del 10 porciento";
                cout<<"\n¿Cuál es el monto de tu compra?: ";
                cin>>totalPagar;
                descuento = totalPagar * 0.15;
                totalPagarDescuento = totalPagar - descuento;
	}
	else {
		cout<<"Número inválido";
	}
	cout<<"El total a pagar es de: "<<totalPagarDescuento<<" pesos";

}
