/*
* Autor: Fatima Azucena MC
* Fecha: 20_01_23
* Correo: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
using namespace std;
int main (){

	//Declaración de variables
	int numero;
	int doble;
	int tercera;
	int mitad;

	cout<<"Digite un número (0-200): ";
	cin>>numero;

	if ( ( numero > 0 ) && ( numero <= 200 ) ){
		doble = numero * 2;
		tercera = doble / 3;
		mitad = tercera / 2;
		cout<<"La mitad de la tercera parte de: "<<numero<<" es: "<<mitad<<"\n";
	}
	else {
		cout<<"Número inválido"<<"\n";
	}

}
