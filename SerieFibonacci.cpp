// Autor: Fátima Azucena MC
// Fecha: 30_01_2023
// Correo: fatimaazucenamartinez274@gmail.com

#include <iostream>
#include <stdlib.h>
using namespace std;

int main(){
	int numero;
	int x = 0;
	int y = 1;
	int z = 1;
	int i;

	cout<<"Digite el número en dónde desea que la serie de fibonacci termine: ";
	cin>>numero;

	for ( i = 1; i < numero; i++ ){
		z = x + y;
		cout<<z<<" ";
		x = y;
		y = z;
	}

	cout<<"\n";
	system("pause");
	return 0;
}


